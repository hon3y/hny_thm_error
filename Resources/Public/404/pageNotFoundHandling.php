<?php

class user_pageNotFound {
    function pageNotFound() {
        //global $GLOBALS['TYPO3_CONF_VARS'];

        $url = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL').'index.php?id=8';

        $_buffer = $this->loadPage($url);
        echo $_buffer;
    }

    function loadPage($url) {
        $agent = "TYPO3 pageNotFoundFunction v1.0";
        $header[] = "Accept: text/vnd.wap.wml,*.*";
        $ch = curl_init($url);

        if ($ch) {
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $tmp = curl_exec($ch);

            if(FALSE == $tmp) {
                print_r(curl_error($ch));
            }

            curl_close($ch);
        }
        return $tmp;
    }
}