<?php
namespace HIVE\HiveThmError\Utility;

/**
 * Class PageNotFoundHandling
 * @package HIVE\HiveThmError\Utitlity
 */
class PageNotFoundHandling {

    /**
     * The TYPO3 page repository. Used for language and workspace overlay
     *
     * @var \TYPO3\CMS\Frontend\Page\PageRepository
     * @inject
     */
    protected $pageRepository;

    /**
     * @return \TYPO3\CMS\Frontend\Page\PageRepository
     */
    protected function getPageRepository()
    {
        if (!$this->pageRepository instanceof \TYPO3\CMS\Frontend\Page\PageRepository) {

            $this->pageRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\Page\PageRepository::class);

        }

        return $this->pageRepository;
    }

    /**
     */
    public function pageNotFound() {

        // Fallback Homepage
        $url = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL');

        // Fallback 404
        if (isset ($GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_redirectFallback'])) {

            $url = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL') . $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_redirectFallback'];

        }

        if (isset($GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_redirectPageID'])) {

            $iId = intval($GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_redirectPageID']);

            if (is_int($iId)) {

                $aRow = $this->getPageRepository()->getPage(intval($GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_redirectPageID']));
                if (!empty($aRow) and $aRow['hidden'] == "0" and $aRow['deleted'] == "0") {
                    $url = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL').'/index.php?id=' . $iId;

                }

            }
        }

        $_buffer = $this->loadPage($url);
        echo $_buffer;

    }

    /**
     * @param $url
     * @return mixed
     */
    function loadPage($url) {
        $agent = "TYPO3 pageNotFoundFunction v1.0";
        $header[] = "Accept: text/vnd.wap.wml,*.*";
        $ch = curl_init($url);

        if ($ch) {
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $tmp = curl_exec($ch);

            if(FALSE == $tmp) {
                print_r(curl_error($ch));
            }

            curl_close($ch);
        }
        return $tmp;
    }

}
?>