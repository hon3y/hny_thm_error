<?php
namespace HIVE\HiveThmError\Utility;

/**
 * Class PageUnavailable
 * @package HIVE\HiveThmError\Utitlity
 */
class PageUnavailableHandling {

    /**
     */
    public function pageUnavailable() {

        // Fallback Homepage
        $url = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL');

        // Fallback 503
        if (isset ($GLOBALS['TYPO3_CONF_VARS']['FE']['pageUnavailable_handling_redirectFallback'])) {

            $url = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SITE_URL') . $GLOBALS['TYPO3_CONF_VARS']['FE']['pageUnavailable_handling_redirectFallback'];

        }

        $_buffer = $this->loadPage($url);
        echo $_buffer;

    }

    /**
     * @param $url
     * @return mixed
     */
    function loadPage($url) {
        $agent = "TYPO3 pageNotFoundFunction v1.0";
        $header[] = "Accept: text/vnd.wap.wml,*.*";
        $ch = curl_init($url);

        if ($ch) {
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $tmp = curl_exec($ch);

            if(FALSE == $tmp) {
                print_r(curl_error($ch));
            }

            curl_close($ch);
        }
        return $tmp;
    }

}
?>