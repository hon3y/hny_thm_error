plugin.tx_hive_thm_error {
    settings {
        production {
            includePath {
                public = EXT:hive_thm_error/Resources/Public/
                private = EXT:hive_thm_error/Resources/Private/
                frontend {
                    public = typo3conf/ext/hive_thm_error/Resources/Public/
                }
            }
            error404 = 8
        }
    }
}